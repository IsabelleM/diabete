from dataprep.eda import create_report
from dataprep.datasets import load_dataset
import pandas as pd

df = load_dataset('adult')
create_report(df).show_browser()

df1 = pd.read_csv('diabetes1.csv')
create_report(df1).show_browser()
report1 = create_report(df1)
report1.save('Report df1') 

df2 = pd.read_csv('diabetes2.csv')
create_report(df2).show_browser()
report2 = create_report(df2)
report2.save('report df2')

df3 = pd.read_csv("diabetes_prediction_dataset.csv")
create_report(df3).show_browser
report3 = create_report(df3)
report3.save('report df3')

df4 = pd.read_csv('diabetes_binary_health_indicators_BRFSS2015.csv')
create_report(df4).show_browser()
report4 = create_report(df4)
report4.save('report df4')

df5 = pd.read_csv("diabetes_binary_5050split_health_indicators_BRFSS2015.csv")
create_report(df5).show_browser()
report5 = create_report(df5)
report5.save('report df5')

df6 = pd.read_csv("diabetes_012_health_indicators_BRFSS2015.csv")
create_report(df6).show_browser()
report6 = create_report(df6)
report6.save('report df6')
